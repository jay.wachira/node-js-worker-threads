const {
  Worker,
  parentPort,
  workerData
} = require("worker_threads");

//get the numbers.
const numbers = workerData;
const calculateFactorialWithworker = numArray =>
  numArray.reduce((acc, val) => acc * val, 1);

const result = calculateFactorialWithworker(numbers);
//return results.
parentPort.postMessage(result);