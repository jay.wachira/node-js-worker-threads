const {
  Worker,
  isMainThread,
  parentPort,
  workerData
} = require("worker_threads");
const inquirer = require("inquirer");
const ora = require("ora");
const path = require("path");
const os = require("os");
const userCPUCount = os.cpus().length;
const NS_PER_SEC = 1e9
const workerPath = path.resolve("factorial_worker.js");

const calculateFactorialWithworker = number => {
  if (number === 0) return 1;
  return new Promise(async (resolve, reject) => {
    const numbers = [...new Array(number)].map((_, i) => i + 1);
    const segmentSize = Math.ceil(numbers.length / userCPUCount);
    const segments = [];
    for (let segmentIndex = 0; segmentIndex < userCPUCount; segmentIndex++) {
      const start = segmentIndex * segmentSize;
      const end = start + segmentSize;
      const segment = numbers.slice(start, end);
      segments.push(segment);
    }
    try {
      const results = await Promise.all(
        segments.map(
          segment =>
          new Promise((resolve, reject) => {
            const worker = new Worker(workerPath, {
              workerData: segment
            });
            worker.on("message", resolve);
            worker.on("error", reject);
            worker.on("exit", code => {
              if (code !== 0)
                reject(new Error(`Worker stopped with exit code ${code}`));
            });
          })
        )
      );
      const finalresult = results.reduce((acc, val) => acc * val, 1);
      resolve(finalresult);
    } catch (e) {
      reject(e);
    }
  });
};
const calculateFactorial = number => {
  const numbers = [...new Array(number)].map((_, i) => i + 1);
  return numbers.reduce((acc, val) => acc * val, 1);
}

const benchmark = async (input, factFun, label) => {
  const spinnerMain = ora(`Calculating with ${label}... `).start();
  const startTime = process.hrtime()
  const result = await factFun(input);
  const diffTime = process.hrtime(startTime)
  spinnerMain.succeed(`${label} results - ${result} done in: ${diffTime[0] * NS_PER_SEC + diffTime[1]} nanoseconds`);
}
const run = async () => {
  const {
    digit
  } = await inquirer.prompt([{
    type: "number",
    name: "digit",
    message: "Calculate factorial for -- ",
    default: 10
  }]);
  benchmark(digit, calculateFactorialWithworker, 'Worker')
  benchmark(digit, calculateFactorial, 'Main')
};
run();